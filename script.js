// Logic for Changing the price for Plan And Pick Yearly and Monthly Wise
let switchbtn = document.querySelector("#switch");
let arcadeprice = document.querySelector(".arcade-price");
let advanceprice = document.querySelector(".advance-price");
let proprice = document.querySelector(".pro-price");

// For Pick Add-ons
let priceonline = document.querySelector("#price-online");
let pricestorage = document.querySelector("#price-storage");
let priceprofile = document.querySelector("#price-profile");
let pricePlan = document.querySelectorAll(".price_plan");

let addOnService = document.querySelectorAll(".add-on-service");
let totalBill = 0;

let circle1 = document.querySelector("#circle-1");
let circle2 = document.querySelector("#circle-2");
let circle3 = document.querySelector("#circle-3");
let circle4 = document.querySelector("#circle-4");

function switchPlanHandler() {
  let planFree = document.querySelectorAll(".plan-free");

  pricePlan = document.querySelectorAll(".price_plan");

  console.log(pricePlan);

  if (switchbtn.checked) {
    arcadeprice.innerText = "$90/yr";
    advanceprice.innerText = "$120/yr";
    proprice.innerText = "$150/yr";

    for (let index = 0; index < planFree.length; index++) {
      planFree[index].classList.remove("hide");
    }

    // For Select Plan

    for (let index = 0; index < pricePlan.length; index++) {
      pricePlan[index].value = Number(pricePlan[index].value) * 10;
    }

    // For Pick Add Ons

    for (let index = 0; index < addOnService.length; index++) {
      addOnService[index].value = Number(addOnService[index].value) * 10;
    }

    priceonline.innerText = "+$10/yr";
    pricestorage.innerText = "+$20/yr";
    priceprofile.innerText = "+$20/yr";
  } else {
    arcadeprice.innerText = "$9/mo";
    advanceprice.innerText = "$12/mo";
    proprice.innerText = "$15/mo";

    for (let index = 0; index < planFree.length; index++) {
      planFree[index].classList.add("hide");
    }

    // For Select Plan

    for (let index = 0; index < pricePlan.length; index++) {
      pricePlan[index].value = Number(pricePlan[index].value) / 10;
    }

    // For Pick Add Ons
    for (let index = 0; index < addOnService.length; index++) {
      addOnService[index].value = Number(addOnService[index].value) / 10;
    }

    priceonline.innerText = "+$1/mo";
    pricestorage.innerText = "+$2/mo";
    priceprofile.innerText = "+$2/mo";
  }
}

switchbtn.addEventListener("change", switchPlanHandler);

// Personal Info JS Start
let nextbtn = document.querySelector(".next-btn");
let personalInfo = document.querySelector(".form-next-step");
let selectYourplan = document.querySelector(".select-your-plan");
let pickAddOn = document.querySelector(".pick-add-ons");
let finishingUp = document.querySelector(".Finishing-up");
let userName = document.querySelector("#name");
let email = document.querySelector("#email");
let phoneNo = document.querySelector("#phoneno");
let footerButton = document.querySelector(".footer-button");
let goBackButton = document.querySelector(".goback-btn");
let getSelectedValue;
let thankYou = document.querySelector(".thankYou");
let addOnServicePrice=0;

// Email validation regex
const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

// Phone number validation regex
const phoneRegex = /^\+?[0-9]+(?:[ -]?[0-9]+)*$/;

function step4() {
  pickAddOn.classList.add("hide");
  nextbtn.removeEventListener("click", step4);
  goBackButton.removeEventListener("click", backStep3);

  thankYou.classList.remove("hide");
  finishingUp.classList.add("hide");

  circle3.classList.remove("selected");
  circle3.classList.add("notselected");

  let footerbtn = document.querySelector(".footer-button");

  footerbtn.style.display = "none";
}

function step3() {
  // Add logic for step 3 if needed

  totalBill=0;
  getSelectedValue = document.querySelector('input[name="price_plan"]:checked');

  console.log(getSelectedValue);
  console.log(getSelectedValue.value);
  totalBill += Number(getSelectedValue.value);

  addOnService = document.querySelectorAll(".add-on-service:checked");
  console.log("From step 3", addOnService);

  finishingUp.classList.remove("hide");
  pickAddOn.classList.add("hide");

  circle3.classList.remove("selected");
  circle3.classList.add("notselected");

  circle4.classList.add("selected");
  circle4.classList.remove("notselected");
 
  addOnServicePrice=0;
  
  for (let index = 0; index < addOnService.length; index++) {
    addOnServicePrice += Number(addOnService[index].value);
    
  }

  totalBill+=addOnServicePrice;

  nextbtn.removeEventListener("click", step3);
  goBackButton.removeEventListener("click", backStep2);

  let modeName = document.querySelector(".mode-name");
  let modePrice = document.querySelector(".mode-price");

  // For Selected Plan

  if (switchbtn.checked) {
    // Yearly prices
    if (getSelectedValue.value == "90") {
      modeName.innerText = "Arcade (Yearly)";
      modePrice.innerText = "$90/yr";
     
    } else if (getSelectedValue.value == "120") {
      modeName.innerText = "Advance (Yearly)";
      modePrice.innerText = "$120/yr";
     
    } else {
      modeName.innerText = "Pro (Yearly)";
      modePrice.innerText = "$150/yr";
     
    }
  } else {
    // Monthly Price

    if (getSelectedValue.value == "9") {
      modeName.innerText = "Arcade (Monthly)";
      modePrice.innerText = "$9/mo";
    } else if (getSelectedValue.value == "12") {
      modeName.innerText = "Advance (Monthly)";
      modePrice.innerText = "$12/mo";
    } else {
      modeName.innerText = "Pro (Monthly)";
      modePrice.innerText = "$15/mo";
    }
  }

  console.log("Running till Now");

  let selectOnlineService = document.querySelector(".online-service");
  let selectLargeStorage = document.querySelector(".larger-storage ");
  let selectCustomizableProfile = document.querySelector(
    ".customizable-profile"
  );

  let selectOnlineServicePrice = document.querySelector(
    ".online-service-price"
  );
  let selectLargeStoragePrice = document.querySelector(
    ".larger-storage-price "
  );
  let selectCustomizablePrice = document.querySelector(
    ".customizable-profile-price"
  );

  selectOnlineService.classList.add("hide");
  selectLargeStorage.classList.add("hide");
  selectCustomizableProfile.classList.add("hide");

  if (switchbtn.checked) {
    // yearly

    for (let index = 0; index < addOnService.length; index++) {
      console.log("id", addOnService[index].id);
      console.log("name", addOnService[index].name);

      if (addOnService[index].name == "onlineservice") {
        selectOnlineService.classList.remove("hide");
        selectOnlineServicePrice.innerText = "$10/yr";
      } else if (addOnService[index].name == "localstorage") {
        selectLargeStorage.classList.remove("hide");
        selectLargeStoragePrice.innerText = "$20/yr";
      } else {
        selectCustomizableProfile.classList.remove("hide");
        selectCustomizablePrice.innerText = "$20/yr";
      }
    }
  } else {
    // Montly
    for (let index = 0; index < addOnService.length; index++) {
      if (addOnService[index].name == "onlineservice") {
        selectOnlineService.classList.remove("hide");
        selectOnlineServicePrice.innerText = "$1/mo";
      } else if (addOnService[index].name == "localstorage") {
        selectLargeStorage.classList.remove("hide");
        selectLargeStoragePrice.innerText = "$2/mo";
      } else {
        selectCustomizableProfile.classList.remove("hide");
        selectCustomizablePrice.innerText = "$2/mo";
      }
    }
  }

  let totalName = document.querySelector(".total-bill-name");
  let totalPrice = document.querySelector(".total-bill-price");

  if (switchbtn.checked) {
    totalName.innerText = "Total (per year)";
    totalPrice.innerText = `+$${totalBill}/yr`;
  } else {
    totalName.innerText = "Total (per month)";
    totalPrice.innerText = `+$${totalBill}/mo`;
  }

  nextbtn.innerText = "Confirm";
  nextbtn.style.backgroundColor = "hsl(243, 100%, 62%)";

  let changebtn = document.querySelector(".change");

  changebtn.addEventListener("click", () => {
    finishingUp.classList.add("hide");
    pickAddOn.classList.add("hide");

    circle4.classList.remove("selected");
    circle4.classList.add("notselected");

    nextbtn.innerText = "Next";
    nextbtn.style.backgroundColor = "hsl(213, 96%, 18%)";
    backStep2();

    nextbtn.removeEventListener("click", step4);
    goBackButton.removeEventListener("click", backStep3);

    return;
  });

  nextbtn.addEventListener("click", step4);
  goBackButton.addEventListener("click", backStep3);
}

function backStep3() {
  pickAddOn.classList.remove("hide");
  finishingUp.classList.add("hide");

  circle4.classList.remove("selected");
  circle4.classList.add("notselected");

  circle3.classList.add("selected");
  circle3.classList.remove("notselected");

  totalBill=0;

  nextbtn.innerText = "Next";
  nextbtn.style.backgroundColor = "hsl(213, 96%, 18%)";

  nextbtn.removeEventListener("click", step4);
  goBackButton.removeEventListener("click", backStep3);
  nextbtn.addEventListener("click", step3);
  goBackButton.addEventListener("click", backStep2);
}

function step2Handler() {
  getSelectedValue = document.querySelector('input[name="price_plan"]:checked');
  console.log(getSelectedValue);
  console.log(getSelectedValue.value);
  console.log(getSelectedValue.id);
  if (getSelectedValue == null) {
    alert("Please Select a Plan");
    return;
  } else {
    
    pickAddOn = document.querySelector(".pick-add-ons");
    selectYourplan = document.querySelector(".select-your-plan");
    pickAddOn.classList.remove("hide");
    selectYourplan.classList.add("hide");

    circle2.classList.remove("selected");
    circle2.classList.add("notselected");

    circle3.classList.add("selected");
    circle3.classList.remove("notselected");

    nextbtn.removeEventListener("click", step1Handler);
    goBackButton.removeEventListener("click", backStep1);
    nextbtn.addEventListener("click", step3);
    goBackButton.addEventListener("click", backStep2);
  }
}

function backStep2() {
  pickAddOn.classList.add("hide");
  selectYourplan.classList.remove("hide");

  nextbtn.removeEventListener("click", step3);
  goBackButton.removeEventListener("click", backStep2);
  goBackButton.addEventListener("click", backStep1);
  nextbtn.addEventListener("click", step2Handler);

  circle3.classList.remove("selected");
  circle3.classList.add("notselected");

  circle2.classList.add("selected");
  circle2.classList.remove("notselected");

  totalBill=0;

}

function step1Handler() {
  userName = document.querySelector("#name");
  email = document.querySelector("#email");
  phoneNo = document.querySelector("#phoneno");

  userName = userName.value;
  email = email.value;
  phoneNo = phoneNo.value;

  let Err = document.querySelectorAll(".err");

  if (userName == "") {
    Err[0].classList.remove("hide");
    return;
  } else {
    Err[0].classList.add("hide");
  }

  if (email == "") {
    Err[1].classList.remove("hide");
    return;
  } else {
    Err[1].classList.add("hide");
  }

  if (phoneNo == "") {
    Err[2].classList.remove("hide");
    return;
  } else {
    Err[2].classList.add("hide");
  }

  if (emailRegex.test(email) && phoneRegex.test(phoneNo)) {
    personalInfo.classList.add("hide");
    selectYourplan.classList.remove("hide");

    circle1.classList.remove("selected");
    circle1.classList.add("notselected");

    circle2.classList.add("selected");
    circle2.classList.remove("notselected");

    footerButton.classList.add("next-btn-right");
    goBackButton.classList.remove("hide");

    nextbtn.removeEventListener("click", step1Handler);
    nextbtn.addEventListener("click", step2Handler);
    goBackButton.addEventListener("click", backStep1);
  } else {
    console.log("some error occurred");
    return;
  }
}

function backStep1() {
  personalInfo.classList.remove("hide");
  selectYourplan.classList.add("hide");

  footerButton.classList.remove("next-btn-right");
  goBackButton.classList.add("hide");

  nextbtn.removeEventListener("click", step2Handler);
  goBackButton.removeEventListener("click", backStep1);

  nextbtn.addEventListener("click", step1Handler);

  circle2.classList.remove("selected");
  circle2.classList.add("notselected");

  circle1.classList.add("selected");
  circle1.classList.remove("notselected");
}

document.addEventListener("DOMContentLoaded", () => {
  nextbtn.addEventListener("click", step1Handler);

  circle1.classList.add("selected");
});
